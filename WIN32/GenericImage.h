#pragma once

#include <Windows.h>

class GenericImage
{
protected:
	UINT  m_w;
	UINT  m_h;
        BYTE *m_data;

public:
	inline UINT GetW() { return m_w; }
	inline UINT GetH() { return m_h; }
	inline BYTE* GetData() { return m_data; }

	GenericImage();
	virtual void Draw(HWND) = 0;
	virtual void Prepare(HWND, UINT, UINT) = 0;
};