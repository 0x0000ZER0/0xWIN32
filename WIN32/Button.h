#pragma once

#include <string>

#include "Item.h"

using namespace std;

class Button : public Item
{
private:
        wstring &m_text;
        int     m_x;        
        int     m_y;        
        int     m_w;        
        int     m_h;  

public:
        Button(int, wstring&, int, int, int, int);      

        inline wstring GetText() { return m_text; }
        inline int GetX() { return m_x; }
        inline int GetY() { return m_y; }
        inline int GetW() { return m_w; }
        inline int GetH() { return m_h; }
};