#pragma once

#include "GenericFilter.h"

class BoxBlur : public GenericFilter
{
private:
	INT m_radius;

public:
	inline void SetRadius(INT radius) { m_radius = radius; }

	BoxBlur(GenericImage*);
	void Filter();
};