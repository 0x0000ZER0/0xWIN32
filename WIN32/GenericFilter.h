#pragma once

#include "Image.h"

class GenericFilter
{
protected:
	GenericImage *m_img;

public:
	GenericFilter(GenericImage*);
	virtual void Filter() = 0;
};