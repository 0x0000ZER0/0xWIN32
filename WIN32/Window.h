#pragma once
#include <windows.h>
#include <string>
#include <vector>

#include "Button.h"
#include "RadioButton.h"
#include "Slider.h"
#include "ImageBox.h"
#include "GenericFilter.h"

using namespace std;

class Window 
{
private:
        wstring        m_name;
        HWND           m_hnd;
        vector<Item>   m_children;
        GenericFilter *m_filter;
        GenericImage  *m_image;

public:
        inline GenericImage* GetImage() { return m_image; }
        inline GenericFilter* GetFilter() { return m_filter; }

        template<typename FTYPE>
        void SetFilter() { 
                if (m_filter != NULL) 
                        delete m_filter;
                m_filter = new FTYPE(m_image);  
        }

        Window(int, int);
        ~Window();

        void Show();
        void AddButton(Button&);
        void AddRadioButtons(vector<RadioButton>&);
        void AddSlider(Slider&);
        void AddImageBox(ImageBox&);
private:
        static LRESULT CALLBACK Listen(HWND, UINT, WPARAM, LPARAM);
};