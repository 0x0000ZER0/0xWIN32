#pragma once

#include "Item.h"

class ImageBox : public Item
{
private:
	int m_x;
	int m_y;

public:
	ImageBox(int, int, int);

	inline int GetX() { return m_x; }
	inline int GetY() { return m_y; }
};