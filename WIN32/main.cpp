#include "Window.h"

int WINAPI WinMain(_In_     HINSTANCE curr, 
                   _In_opt_ HINSTANCE prev,
                   _In_     LPSTR     args, 
                   _In_     int       mode) 
{
        const int width  = 800;
        const int height = 600;
        Window win(width, height);

        wstring button_text = L"Run Filter";
        Button button(0, button_text, width / 2 - (100 / 2), 50, 100, 30);
        win.AddButton(button);

        wstring rbutton1_text = L"Box Blur";
        wstring rbutton2_text = L"B and W";
        RadioButton rbutton1(1, rbutton1_text, 10, 10);
        RadioButton rbutton2(2, rbutton2_text, 120, 10);

        vector<RadioButton> rbuttons;
        rbuttons.push_back(rbutton1);
        rbuttons.push_back(rbutton2);
        win.AddRadioButtons(rbuttons);

        Slider slider(3, 0, 50);
        win.AddSlider(slider);
        
        ImageBox box(4, 10, 150); 
        win.AddImageBox(box);

        win.Show();
        return 0;
}