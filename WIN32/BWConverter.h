#pragma once

#include "GenericFilter.h"

class BWConverter : public GenericFilter
{
public:
	BWConverter(GenericImage*);
	void Filter();
};