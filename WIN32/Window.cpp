#include "Window.h"

#include <iostream>
#include <Commctrl.h>

#include "Image.h"
#include "BoxBlur.h"
#include "BWConverter.h"

using namespace std;

LRESULT CALLBACK Window::Listen(HWND hnd, UINT msg, WPARAM wprm, LPARAM lprm) 
{
        const int userData = -21;
        const int button   = 0;
        const int rbutton1 = 1;
        const int rbutton2 = 2;
        const int sliderId = 3;
        const int imgBox   = 4;

        switch(msg) 
        {
        case WM_CREATE:
                SetWindowLongPtr(hnd, GWLP_USERDATA, (LONG_PTR)((CREATESTRUCT*)lprm)->lpCreateParams);
                SetWindowPos(hnd, 0, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER);
                break;
        case WM_COMMAND:
                switch (wprm)
                {
                case rbutton2: 
                {
                        HWND slider = GetDlgItem(hnd, sliderId);
                        ShowWindow(slider, SW_HIDE);

                        auto *_this = (Window*)GetWindowLongPtr(hnd, userData);
                        _this->SetFilter<BWConverter>();
                        break;
                } 
                case rbutton1:
                {
                        HWND slider = GetDlgItem(hnd, sliderId);
                        ShowWindow(slider, SW_SHOW);

                        auto *_this = (Window*)GetWindowLongPtr(hnd, userData);
                        _this->SetFilter<BoxBlur>();
                        break;
                }
                case button:
                        HWND box = GetDlgItem(hnd, imgBox);
                        RECT rect;
                        GetWindowRect(box, &rect);

                        auto *_this = (Window*)GetWindowLongPtr(hnd, userData);

                        _this->GetImage()->Prepare(box, rect.right - rect.left, 400);
                        if (_this->GetFilter() != NULL)
                                _this->GetFilter()->Filter();
                        _this->GetImage()->Draw(box);
                        break; 
                }
        
                break;
        case WM_HSCROLL:
        {
                HWND slider = GetDlgItem(hnd, sliderId);
                LRESULT value = SendMessage(slider, TBM_GETPOS, 0, 0);

                auto *_this = (Window*)GetWindowLongPtr(hnd, userData);
                auto *curr  = (BoxBlur*)_this->GetFilter();

                if (curr != NULL)
                        curr->SetRadius(value / 20 + 1);
                break;
        }        
        case WM_PAINT:
        {
                HWND box = GetDlgItem(hnd, 4);
                RECT rect;
                GetWindowRect(box, &rect);

                auto *_this = (Window*)GetWindowLongPtr(hnd, userData);
                BWConverter filter(_this->GetImage());

                _this->GetImage()->Prepare(box, rect.right - rect.left, 400);
                _this->GetImage()->Draw(box);
 
                return DefWindowProc(hnd, msg, wprm, lprm);
        }
        case WM_CLOSE:
                DestroyWindow(hnd);
                break;
        case WM_DESTROY:
                PostQuitMessage(0);
                break;
        default:
                return DefWindowProc(hnd, msg, wprm, lprm);
        }
}

Window::Window(int w, int h)
{
        m_name = L"WINDOW";

        WNDCLASSEX wc;
        wc.cbSize        = sizeof (WNDCLASSEX);
        wc.lpszClassName = m_name.c_str();
        wc.lpfnWndProc   = Listen;
        wc.style         = 0;
        wc.cbClsExtra    = 0;
        wc.cbWndExtra    = 0;
        wc.hInstance     = NULL;
        wc.hIcon         = NULL;
        wc.hCursor       = NULL;
        wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
        wc.lpszMenuName  = NULL;
        wc.hIconSm       = NULL;

        ATOM res;
        res = RegisterClassEx(&wc);
        if (res == 0) 
        {
                wcerr << L"ERR: failed to register the class [" << m_name << L"]" << endl;
                ExitProcess(1);
        }

        m_hnd = CreateWindowEx(WS_EX_CLIENTEDGE, m_name.c_str(), L"WIN32 API", WS_OVERLAPPEDWINDOW,
                               CW_USEDEFAULT, CW_USEDEFAULT, w, h,
                               NULL, NULL, NULL, this);
        if (m_hnd == NULL) 
        {
                wcerr << L"ERR: failed to create a window with the type [" << m_name << L"]" << endl;
                ExitProcess(1);
        }

        m_image = new Image<PixelType::RGB>(L"C:\\Users\\073056\\Desktop\\img.png");
}

Window::~Window() 
{
        HWND hnd;
        for (Item &item : m_children) 
        {
                hnd = GetDlgItem(m_hnd, item.GetID());
                DestroyWindow(hnd);
        }

        DestroyWindow(m_hnd);
        UnregisterClass(m_name.c_str(), NULL);

        delete m_image;
        delete m_filter;
}

void Window::Show() 
{
        ShowWindow(m_hnd, SW_SHOW);
        UpdateWindow(m_hnd);

        MSG msg;
        while(GetMessage(&msg, NULL, 0, 0) > 0) 
        {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
        }
}

void Window::AddButton(Button& button) 
{       
        const wchar_t name[] = L"BUTTON";

        HWND hnd;
        hnd = CreateWindow(name, button.GetText().c_str(),
                           WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
                           button.GetX(), button.GetY(), button.GetW(), button.GetH(),
                           m_hnd, (HMENU)button.GetID(), NULL, NULL);
        if (hnd == NULL) 
        {
                wcerr << L"ERR: failed to create a button." << endl;
                ExitProcess(1);        
        }

        m_children.push_back(button);
}

void Window::AddRadioButtons(vector<RadioButton>& buttons) 
{       
        const wchar_t name[] = L"BUTTON";

        HWND hnd;
        hnd = CreateWindowEx(WS_EX_WINDOWEDGE, name, buttons[0].GetText().c_str(),
                             WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON | WS_GROUP, 
                             buttons[0].GetX(), buttons[0].GetY(), 100, 20,
                             m_hnd, (HMENU)buttons[0].GetID(), NULL, NULL);
        if (hnd == NULL) 
        {
                wcerr << L"ERR: failed to create a radio button [GROUP START]." << endl;
                ExitProcess(1);        
        }
        m_children.push_back(buttons[0]);

        for (size_t i = 1; i < buttons.size(); ++i)
        {
                hnd = CreateWindowEx(WS_EX_WINDOWEDGE, name, buttons[i].GetText().c_str(),
                                     WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON, 
                                     buttons[i].GetX(), buttons[i].GetY(), 100, 20,
                                     m_hnd, (HMENU)buttons[i].GetID(), NULL, NULL);
                if (hnd == NULL) 
                {
                        wcerr << L"ERR: failed to create a radio button." << endl;
                        ExitProcess(1);        
                }
                m_children.push_back(buttons[i]);
        }
}

void Window::AddSlider(Slider& slider) 
{
        const wchar_t name[] = L"msctls_trackbar32";

        HWND hnd;
        hnd = CreateWindow(name, NULL, 
                           WS_CHILD | WS_VISIBLE,
                           slider.GetX(), slider.GetY(), 200, 30, 
                           m_hnd, (HMENU)slider.GetID(), NULL, NULL);
        if (hnd == NULL) 
        {
                wcerr << L"ERR: failed to create a slider." << endl;
                ExitProcess(1);        
        }

        m_children.push_back(slider);
}

void Window::AddImageBox(ImageBox& box) 
{
        HWND hnd;
        hnd = CreateWindow(L"STATIC", L"", WS_VISIBLE | WS_CHILD , 
                           box.GetX(), box.GetY(), 760, 0, 
                           m_hnd, (HMENU)box.GetID(), NULL, NULL);

        if (hnd == NULL) 
        {
                wcerr << L"ERR: failed to create a slider." << endl;
                ExitProcess(1);        
        }

        m_children.push_back(box);
}