#pragma once

#include "GenericImage.h"

#include <string>
#include <iostream>
#include <atlbase.h>
#include <wincodec.h>

using namespace std;

enum PixelType 
{
        BGR  = 0x0,
        RGB  = 0x1,
        BGRA = 0x2,
        RGBA = 0x3,
};

template<PixelType PIXELTYPE>
class Image : public GenericImage
{
private:
        IWICImagingFactory    *m_factory;
        IWICBitmapFrameDecode *m_frame;
        IWICFormatConverter   *m_converter;
        HDC                    m_hdc;
        BITMAPINFO             m_info;
        HBITMAP                m_dib;

public:
        Image(wstring path)
                : m_hdc(NULL), m_dib(NULL) 
        {
                HRESULT hr;

                hr = CoInitialize(NULL);
                if (hr != S_OK)
                {
                        wcerr << L"ERR: could not initialize the COM." << endl;
                        ExitProcess(1);    
                }
                
                hr = CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&m_factory));
                if (hr != S_OK)
                {
                        wcerr << L"ERR: failed to create a WIC factory." << endl;
                        ExitProcess(1);    
                }
                
                IWICBitmapDecoder *decoder;
                hr = m_factory->CreateDecoderFromFilename(path.c_str(), NULL, GENERIC_READ, WICDecodeMetadataCacheOnLoad, &decoder);
                if (hr != S_OK)
                {
                        wcerr << L"ERR: failed to create the decoder." << endl;
                        ExitProcess(1);    
                }

                hr = decoder->GetFrame(0, &m_frame);
                if (hr != S_OK)
                {
                        wcerr << L"ERR: failed to retrieve the first frame." << endl;
                        ExitProcess(1);    
                }

                hr = m_frame->GetSize(&m_w, &m_h);
                if (hr != S_OK)
                {
                        wcerr << L"ERR: failed to retrieve the frame size." << endl;
                        ExitProcess(1);    
                }

                hr = m_factory->CreateFormatConverter(&m_converter);
                if (hr != S_OK)
                {
                        wcerr << L"ERR: failed to create the converter." << endl;
                        ExitProcess(1);    
                }

                static GUID typeTable[] = {
                        GUID_WICPixelFormat32bppBGR,
                        GUID_WICPixelFormat32bppRGB,
                        GUID_WICPixelFormat32bppBGRA,
                        GUID_WICPixelFormat32bppRGBA,
                };

                hr = m_converter->Initialize(m_frame, typeTable[PIXELTYPE], WICBitmapDitherTypeNone, NULL, 0.0, WICBitmapPaletteTypeCustom);
                if (hr != S_OK)
                {
                        wcerr << L"ERR: could not initialize the converter." << endl;
                        ExitProcess(1);    
                }
        }
        ~Image()
        {
                CoUninitialize();
        }

        void Draw(HWND hnd)
        {
                HDC whdc = GetDC(hnd);
                SetDIBitsToDevice(whdc, 0, 0, m_w, m_h, 0, 0, 0, m_h, m_data, &m_info, DIB_RGB_COLORS);

                ReleaseDC(NULL, whdc);
                ReleaseDC(NULL, m_hdc);
                DeleteObject(m_dib);
        }
        void Prepare(HWND hnd, UINT w, UINT h) 
        {
                HRESULT hr;

                IWICBitmapScaler *scaler;

                hr = m_factory->CreateBitmapScaler(&scaler);
                if (hr != S_OK)
                {
                        wcerr << L"ERR: failed to create the Scaler." << endl;
                        ExitProcess(1);   
                }

                hr = scaler->Initialize(m_converter, w, h, WICBitmapInterpolationModeFant);
                if (hr != S_OK)
                {
                        wcerr << L"ERR: failed to initialize the Converter." << endl;
                        ExitProcess(1);   
                }

                ZeroMemory(&m_info, sizeof (m_info));
                m_info.bmiHeader.biSize         = sizeof (BITMAPINFOHEADER);
                m_info.bmiHeader.biWidth        = w;
                m_info.bmiHeader.biHeight       = -(LONG)h;
                m_info.bmiHeader.biPlanes       = 1;
                m_info.bmiHeader.biBitCount     = 32;
                m_info.bmiHeader.biCompression  = BI_RGB;

                m_hdc = GetDC(NULL); 
                if (m_hdc == NULL) 
                {
                        wcerr << L"ERR: failed to create the temporary Device Context." << endl;
                        ExitProcess(1);
                }

                m_dib = CreateDIBSection(m_hdc, &m_info, DIB_RGB_COLORS, (void**)&m_data, NULL, 0);
                if (m_data == NULL || m_dib == NULL)
                {
                        ReleaseDC(NULL, m_hdc);
                        wcerr << L"ERR: failed to create the DIB section." << endl;
                        ExitProcess(1);
                }

                m_w = ((w * 32 + 31) >> 5) << 2;
                m_h = h;

                hr = scaler->CopyPixels(NULL, m_w, m_w * h, (BYTE*)m_data);
                if (hr != S_OK)
                {
                        wcerr << L"ERR: could not copy the pixels." << endl;
                        ExitProcess(1);   
                }
        }
};
