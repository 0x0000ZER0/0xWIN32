#include "Slider.h"

Slider::Slider(int id, int x, int y)
        : Item(id), m_x(x), m_y(y)
{}

void Slider::Hide(HWND hnd) 
{
        HWND slider = GetDlgItem(hnd, m_id);
        SetWindowLongPtr(slider, GWL_STYLE, WS_CHILD | WS_VISIBLE);
        RedrawWindow(hnd, NULL, NULL, RDW_INVALIDATE);
}

void Slider::Show(HWND hnd) 
{
        HWND slider = GetDlgItem(hnd, m_id);
        SetWindowLongPtr(slider, GWL_STYLE, WS_CHILD);
        RedrawWindow(hnd, NULL, NULL, RDW_INVALIDATE);
}

