#pragma once

#include "Item.h"

#include <Windows.h>

class Slider : public Item
{
private:
	int  m_x;
	int  m_y;

public:
	Slider(int, int, int);

	inline void Hide(HWND);
	inline void Show(HWND);

	inline int GetX() { return m_x; }
	inline int GetY() { return m_y; }
};