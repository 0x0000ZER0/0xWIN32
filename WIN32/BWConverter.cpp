#include "BWConverter.h"

BWConverter::BWConverter(GenericImage *img)
	: GenericFilter(img)
{
}

void BWConverter::Filter()  
{
	UINT  w = m_img->GetW();
	UINT  h = m_img->GetH();
	BYTE *data = m_img->GetData();

        for (UINT i = 0; i < w * h; i += 4) 
        {
                BYTE r = data[i + 0];
                BYTE g = data[i + 1];
                BYTE b = data[i + 2];
                        
                BYTE w = (r / 4  + g / 2 + b / 4 - 10) / 3;

                data[i + 0] = w;
                data[i + 1] = w;
                data[i + 2] = w;
        }		
}