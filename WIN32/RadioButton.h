#pragma once

#include <string>

#include "Item.h"

using namespace std;

class RadioButton : public Item
{
private:
	wstring& m_text;
	int	m_x;
	int	m_y;

public:
	RadioButton(int, wstring&, int, int);

	inline wstring GetText() { return m_text; }
	inline int GetX() { return m_x; }
	inline int GetY() { return m_y; }
};