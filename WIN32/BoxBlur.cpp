#include "BoxBlur.h"

#include <thread>

using namespace std;

BoxBlur::BoxBlur(GenericImage *img)
	: GenericFilter(img), m_radius(1)
{ }

void BoxBlur::Filter()
{
	BYTE *data = m_img->GetData();

        //  _this = 8 bytes
        //   data = 8 bytes   
        //   off  = 4 bytes 
        //   
        //  L1 Cache = 64 Bytes  
        //  => 64 - (2 * 8 + 4) = 64 - 20 = 44
        BYTE pad[44];
	auto proc = [=](BoxBlur *_this, BYTE *data, UINT off, BYTE *pad) 
        {
                INT  w = _this->m_img->GetW();
	        INT  h = _this->m_img->GetH();
                INT  r = _this->m_radius;
                
                BYTE *buff = new BYTE[w * h];

                memcpy_s(buff, (rsize_t)(w * h), data, (rsize_t)(w * h));

                // [c1, c2, c3, cX] [c1, c2, c3, cX] [c1, c2, c3, cX]
                // [c1, c2, c3, cX] {c1, c2, c3, cX} [c1, c2, c3, cX]
                // [c1, c2, c3, cX] [c1, c2, c3, cX] [c1, c2, c3, cX]
                for (INT y = 0; y < h; y += 4) 
                for (INT x = 0; x < w; x += 4) 
                {
                        INT sum = 0;

                        INT xc = x + off;
                        INT yc = y + off;

                        for (INT i = -r; i <= r; ++i)
                        for (INT j = -r; j <= r; ++j)
                        {
                                INT y0 = yc + i * 4;
                                INT x0 = xc + j * 4;
                                
                                if ((x0 >= 0 && x0 < w) && (y0 >= 0 && y0 < h))
                                        sum += buff[x0 + y0 * w];
                        }

                        data[xc + yc * w] = sum / 9;        
                }       

                delete[] buff;
        };

        thread t1(proc, this, data, 0, pad);
        thread t2(proc, this, data, 1, pad);
        thread t3(proc, this, data, 2, pad);

        t1.join();
        t2.join();
        t3.join();
}
